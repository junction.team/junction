'use strict';

const internals = {
  mediaStream: null
};

module.exports = {
  async start() {

    internals.mediaStream = internals.mediaStream || await navigator.mediaDevices.getUserMedia({
      audio: true
    });

    return internals.mediaStream;
  },

  stop() {

    if (!internals.mediaStream) {
      return;
    }

    for (const track of internals.mediaStream.getAudioTracks()) {
      track.stop();
    }

    internals.mediaStream = null;
  }
};
