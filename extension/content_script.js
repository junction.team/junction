'use strict';

(() => {

  const io = require('socket.io-client');
  const Peers = require('./peers');
  const Media = require('./media');

  const internals = {

    kSocketUrl: 'https://junction.unlimited.pizza/',

    port: null,
    socket: null,
    peers: 0,

    onMessage(message) {
      internals[message.action](message.data);
    },

    async joinAudioCall(data) {

      internals.tada = data.tada; // Keeping for fun

      try {
        const mediaStream = await Media.start();

        internals.socket = io(internals.kSocketUrl, {
          transports: ['websocket']
        });

        internals.socket.on('error', function(error) {

          console.error('GENERAL ERROR', error);
        });

        internals.socket.on('connect_error', function(error) {

          console.error('CONNNECT ERROR', error);
        });

        internals.socket.on('connect', function() {

          console.log("Connected to signaling server, group: ", data.currentUrl);
          internals.socket.emit('join', {
            'url': data.currentUrl,
          });
        });

        internals.socket.on('disconnect', function() {

          console.log("disconnected from signaling server");
        });

        internals.socket.on('addPeer', function(data) {

          console.log(data);
          Peers.add('id-'+Peers.count(), internals.tada);
          console.log(`There are now ${Peers.count()} participants`);
        });

        internals.socket.on('removePeer', function() {

          Peers.remove('id-'+(Peers.count() - 1)); // This is only for testing, don't use count to remove ids.
          console.log(`There are now ${Peers.count()} participants`);
        });
      }
      catch (err) {

        internals.port.postMessage({
          action: 'error'
        });
        internals.port.disconnect();
      }
    },

    hangUp() {

      Peers.reset();
      Media.stop();
      internals.socket.close();
      internals.port.disconnect();
    }
  };

  internals.port = chrome.runtime.connect({ name:"content" });
  internals.port.onMessage.addListener(internals.onMessage);
})();

// Indicates to the background script that we executed correctly
true;
