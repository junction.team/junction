# Junction

Connect people through any URL

## Extension

First, make sure to build the plugin. From the extension's root you can call:

```
$ yarn install && yarn build
```

### Testing on Firefox

In order to test on firefox, first go to `about:debugging`, then click
on the `This Firefox` option. Then click on `Load Temporary Add-On` and
point the browser to the `extension/manifest.json` file.

This will enable the extension and will allow you to use the inspector to debug.

### Testing on Chrome

In order to test on chrome, first go to `chrome://extensions/`. Make sure
`Developer mode` is enabled. Then click `Load Unpacked` and point the browser
to the `extension` directory.
