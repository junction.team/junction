function addPeer(peerId, shouldCreateOffer) {
    return {
        peer_id: peerId,
        should_create_offer: shouldCreateOffer,
    }
}

function removePeer(peerId) {
    return {
        peer_id: peerId,
    }
}

function SessionDescriptionReceived(peerId, sessionDescription) {
    return {
        peer_id: peerId,
        session_description: sessionDescription,
    }
}

function ICECandidateReceived(peerId, ICECandidate) {
    return {
        peer_id: peerId,
        ice_candidate: ICECandidate,
    }
}

module.exports = {
    types: {
        ADD_PEER: 'addPeer',
        REMOVE_PEER: 'removePeer',
        RELAY_SESSION_DESCRIPTION: 'relaySessionDescription',
        RELAY_ICE_CANDIDATE: 'relayICECandidate',
        ICE_CANDIDATE_RECEIVED: 'ICECandidateReceived',
        SESSION_DESCRIPTION_RECEIVED: 'SessionDescriptionReceived',
    },
    addPeer,
    removePeer,
    SessionDescriptionReceived: SessionDescriptionReceived,
    ICECandidateReceived: ICECandidateReceived,
}