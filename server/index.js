const PORT = 8000;

const express = require('express');
const http = require('http');
const main = express();
const server = http.createServer(main);
const io  = require('socket.io').listen(server);
const events = require('./events');

server.listen(PORT, null, function() {
    console.log("Listening on port " + PORT);
});

const sockets = {};

io.sockets.on('connection', (socket) => {
    sockets[socket.id] = socket;
    const me = socket.id;

    console.log(`[CONNECT] New client connected with ID ${me}`);

    socket.on('join', (data) => {
        Object.entries(io.in(data.url).sockets).forEach(([peerId, peer]) => {
            if (peerId !== me) {
                peer.emit(events.types.ADD_PEER, events.addPeer(me, false));
                socket.emit(events.types.ADD_PEER, events.addPeer(peerId, true));
            }
        });

        socket.join(data.url);
        console.log(`[CONNECT] Client ${me} added to room ${data.url}`);
    });

    socket.on('disconnecting', () => {
        const rooms = Object.keys(socket.rooms);
        rooms.forEach(room => io.to(room).emit(events.types.REMOVE_PEER, events.removePeer(me)));

        console.log(`[DISCONNECT] Client ${me} has disconnected and has been removed from all rooms`);
    });

    socket.on('relayICECandidate', (data) => {
        Object.keys(io.in(data.url).sockets).forEach(peer =>
            peer.emit(events.types.ICE_CANDIDATE_RECEIVED, events.ICECandidateReceived(me, data.ice_candidate)));

        console.log(`[RELAY_ICE_CANDIDATE] ICE candidate for client ${me} has been relayed to all peers`);
    });

    socket.on('relaySessionDescription', (data) => {
        Object.keys(io.in(data.url).sockets).forEach(peer =>
            peer.emit(events.types.SESSION_DESCRIPTION_RECEIVED, events.SessionDescriptionReceived(me, data.session_description)));

        console.log(`[RELAY_SESSION_DESCRIPTION] Session description for client ${me} has been relayed to all peers`);
    });
});